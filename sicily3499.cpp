//题目网址:
//http://soj.me/3499

//题目分析:
//很水超水，减去数据中最大和最小求平均值即可，可是状态不佳啊今天

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int t, m;
    int max, min, score;
    double avg;
    cin >> t;
    while (t--) {
        avg = 0.0;
        cin >> m;
        max = -1;
        min = 101;
        for (int i = 0; i < m; i++) {
            cin >> score;
            avg += double(score);
            if (score > max) { max = score; }
            if (score < min) { min = score; }
        }
        avg = avg - double(max + min);
        cout << setiosflags(ios::fixed)
             << setprecision(2) << avg / (m-2) << endl;
    }
    
    return 0;
}                                 